package org.tensorflow.demo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by Mikhail on 17.10.2017.
 */

public class MainClassifierActivity extends Activity {

    public final static String CA_EXTRA_MESSAGE = "CA_EXTRA_MESSAGE";
    public final static String EXTRA_FIRST_NN_MODEL = "EXTRA_FIRST_NN_MODEL";
    public final static String EXTRA_SECOND_NN_MODEL = "EXTRA_SECOND_NN_MODEL";

    Button firstClassifier;
    Button secondClassifier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.classifier_main_activity);
        firstClassifier = (Button) findViewById(R.id.first_model);
        secondClassifier = (Button) findViewById(R.id.second_model);

        firstClassifier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ClassifierActivity.class);
                intent.putExtra(CA_EXTRA_MESSAGE, EXTRA_FIRST_NN_MODEL);
                startActivity(intent);
            }
        });

        secondClassifier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ClassifierActivity.class);
                intent.putExtra(CA_EXTRA_MESSAGE, EXTRA_SECOND_NN_MODEL);
                startActivity(intent);
            }
        });
    }
}
